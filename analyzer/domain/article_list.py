from analyzer.domain.article import Article

class ArticleList(object):
    def __init__(self, text_list):
        self.text_list = text_list

    def get_articles(self):
        article_list = []
        for text in self.text_list:            
            activity = text[text.find(':') + 1: text.find('Turma:')].strip()
            course = text[text.find('Turma:') + len('Turma:') : text.find('Estudante:')].strip()
            student = text[text.find('Estudante:') + len('Estudante:') : text.find('\r\n\r\n')].strip()
            text = text[text.find('\n\n') + len('\n\n') : ]

            article = Article(activity, course, student, text)

            article_list.append(article)

        return article_list