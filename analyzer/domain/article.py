class Article(object):
    def __init__(self, activity, course, student, text):
        self.activity = activity
        self.course = course
        self.student = student
        self.text = text
        self.lsa_grade = 0
        self.lsa_x_pos = 0
        self.lsa_y_pos = 0