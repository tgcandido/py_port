from analyzer.domain.load_data import LoadData
from analyzer.domain.decomposer_agent import DecomposerAgent
from analyzer.domain.token_concentration_verifier import TokenConcentrationVerifier

class DiagnoseMessage(object):
    def __init__(self, black_board, message, subject):
        black_board.message = message
        load_data = LoadData(black_board, subject)

        decompositor = DecomposerAgent(black_board)
        decompositor.decompose_message()

        tcv = TokenConcentrationVerifier(black_board)
        tcv.verify_tokens_limits()
