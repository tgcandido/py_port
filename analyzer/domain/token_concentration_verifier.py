from analyzer.domain.diagnostic_result import DiagnosticResult

class TokenConcentrationVerifier(object):
    def __init__(self, black_board):
        self.black_board = black_board

    def verify_tokens_limits(self):
        limit_sum = 0
        denominator = 0
        diagnostic_result_list = []

        for term in self.black_board.dangerous_term_list:
            token_term = next((x for x in self.black_board.term_list if x.term == term.term), None) 
            if token_term is not None:
                limit_sum += token_term.frequency
                diagnostic_result = DiagnosticResult(token_term.subject, token_term.text_frequency, 1, 1)
                token_result = next((x for x in diagnostic_result_list if x.subject == diagnostic_result.subject), None) 
                if token_result is not None:
                    token_result.count += 1
                    token_result.frequency_sum = token_result.frequency_sum + token_term.frequency
                    token_result.concentration = token_result.frequency_sum / token_result.count
                else:
                    diagnostic_result_list.append(diagnostic_result)
                
        self.black_board.term_list_result = diagnostic_result_list