from trainning.domain.stop_words import StopWords
from .doc_word_relation import DocWordRelation

class LsaReader(object):
    def __init__(self):
        self.document_hashtable = {}
        self.word_hashtable = {}
        self.doc_word_hashtable = {}
        self.stop_words = StopWords('pt-br')

    def start_document_list_LSA(self, subject):
        if subject not in self.document_hashtable[subject]:
            self.document_hashtable[subject] = 0

    def index_document_LSA(self, word_list, doc_id):
        for word in word_list:
            if not self.stop_words.is_stop_word(word):
                if word not in self.word_hashtable:
                    self.word_hashtable[word] = len(self.word_hashtable)
                
                dwr = DocWordRelation()
                dwr.doc_id = doc_id
                dwr.pal_id = self.word_hashtable[word]


                if dwr not in self.doc_word_hashtable:
                    self.doc_word_hashtable[dwr] = 1
                else:
                    self.doc_word_hashtable[dwr] = self.doc_word_hashtable[dwr] + 1 

lsa_reader_instance = LsaReader()