from .lsa_reader import LsaReader
from .doc_word_relation import DocWordRelation
from .document_index import DocumentIndex
from collections import defaultdict
from .hash_obj import HashObj
import numpy

class LsaMain(object):

    def __init__(self, k=2):
        self.doc_index = {}
        self.k = k

    def index(self, lsa_reader):
        doc_list = lsa_reader.document_hashtable
        word_list = lsa_reader.word_hashtable
        doc_word_list = lsa_reader.doc_word_hashtable

        local_weight_matrix = numpy.zeros(shape=(
            len(word_list),
            len(doc_list)
        ))

        # Cria matriz Termo/Documento. 
        for line in word_list:
            for column in doc_list:
                line_value = word_list[line]
                dwr = DocWordRelation()
                dwr.doc_id = column
                dwr.pal_id = line_value

                if dwr in doc_word_list:
                    local_weight_matrix[line_value][column] = doc_word_list[dwr]
                else:
                    local_weight_matrix[line_value][column] = 0
        

        # calcula o peso dos termos

        global_term_weight = numpy.zeros(shape=len(word_list))
        doc_norm_factors = numpy.zeros(shape=len(doc_list))

        for i in range(0, len(local_weight_matrix)):
            sum = 0
            fi = 0
            for j in range(0, len(local_weight_matrix[i])):
                if local_weight_matrix[i][j] > 0:
                    sum = sum + 1
                    fi = fi + local_weight_matrix[i][j]
            if (sum > 0):
                global_term_weight[i] = numpy.log(len(doc_list) / sum)
            else:
                global_term_weight[i] = 0
        

        #calcular fatores de normalizacao-> Fator de normalizacao por coseno
        for j in range(0, len(local_weight_matrix[0])):
            sum = 0
            for i in range(0, len(local_weight_matrix)):
                sum += numpy.square(
                    global_term_weight[i] * local_weight_matrix[i][j]
                )
            doc_norm_factors[j] = 1 / numpy.sqrt(sum)


        # cria matriz termo/documento ponderada
        mWTDM = numpy.zeros(shape=(len(word_list), len(doc_list)))
        for i in range(0, len(mWTDM)):
            for j in range(0, len(mWTDM[i])):
                mWTDM[i][j] = local_weight_matrix[i][j] * global_term_weight[i] * doc_norm_factors[j]
        
        # calcula SVD
        u, s, v = numpy.linalg.svd(mWTDM)

        s = numpy.diag(s)
        # valor k selecionado para reduzir as dimensoes da matriz termo-documento
        svd_rank = 2 # todo fix

        # reduz vetor que mantem os valores singulares
        sk = numpy.zeros(shape=(svd_rank, svd_rank))
        for i in range(0, svd_rank):
            for j in range(0, svd_rank):
                sk[i][j] = s[i][j]

        # sk inverso
        skinv = numpy.linalg.inv(sk)

        # calcula uk reduzido
        uk = numpy.zeros(shape=(len(u), svd_rank))
        for i in range(0, len(u)):
            for j in range(0, svd_rank):
                uk[i][j] = u[i][j]
        
        doc_index = DocumentIndex()
        doc_index.word_list = defaultdict(list)
        doc_index.doc_list = defaultdict(list)

        for entry in word_list:
            doc_index.word_list[entry] = word_list[entry]

        for entry in doc_list:
            doc_index.doc_list[entry] = doc_list[entry]
        
        doc_index.inverse_ski = skinv
        doc_index.uk = uk
        doc_index.wtdm = mWTDM
        doc_index.v = v

        self.doc_index = doc_index

    
    def calculate_similarities(self):
        m_v = numpy.zeros(shape=(len(self.doc_index.word_list), 1))

        for k in range(0, len(self.doc_index.word_list)):
            m_v[k][0] = self.doc_index.wtdm[k][0]            
        
        mt = numpy.transpose(m_v)
        mt_uk = numpy.dot(mt, self.doc_index.uk)
        m = numpy.dot(mt_uk, self.doc_index.inverse_ski)

        sim_list = []
        for l in range(1, len(self.doc_index.doc_list)):
            sim_list.append([0,0,0])
            d_v = numpy.zeros(shape=(len(self.doc_index.word_list), 1))
            for k in range(0, len(self.doc_index.word_list)):
                d_v[k][0] = self.doc_index.wtdm[k][l]

            dt = numpy.transpose(d_v)
            dt_uk = numpy.dot(dt, self.doc_index.uk)
            d = numpy.dot(dt_uk, self.doc_index.inverse_ski)

            sim_list[l-1][0] = self._calculate_cosine_sim(d[0], m[0])

            if self.k == 2:
                sim_list[l - 1][1] = self.doc_index.v[l - 1][0]
                sim_list[l - 1][2] = self.doc_index.v[l - 1][1]
            else:
                sim_list[l - 1][1] = self.doc_index.v[l - 1][1] 
                sim_list[l - 1][2] = self.doc_index.v[l - 1][2]

            
        return sim_list

    def _calculate_cosine_sim(self, q, d):
        if len(q) != len(d):
            raise Exception()
        
        numer = 0
        denom = 1
        d1 = 0
        d2 = 0
        resultado = 0

        for m in range(0, len(q)):
            numer += (q[m] * d[m])
            d1 += (q[m] * q[m])
            d2 += (d[m] * d[m])
        
        denom = numpy.sqrt(d1) * numpy.sqrt(d2)
        resultado = (numer / denom)

        return resultado