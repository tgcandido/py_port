from .lsa_main import LsaMain
from analyzer.domain.black_board import BlackBoard
from analyzer.domain.article import Article
from analyzer.domain.diagnostic import DiagnoseMessage
from .lsa_reader import lsa_reader_instance

def diagnose_article_list(article_list):
    black_board_list = []
    lsa_reader = lsa_reader_instance
    lsa_main = LsaMain()
    articles = article_list.get_articles()

    for article in articles:
        black_board = BlackBoard()
        diagnostics = DiagnoseMessage(black_board, article.text, article.activity)
        black_board.article = article
        black_board_list.append(black_board)
    
    _lsa_start(black_board_list)
    lsa_main.index(lsa_reader)
    similarity = lsa_main.calculate_similarities()

    if not similarity:
        raise Exception()
    else:
        for i in range(0, len(articles)):
            articles[i].lsa_grade = similarity[i][0]
            articles[i].lsa_x_pos = similarity[i][1]
            articles[i].lsa_y_pos = similarity[i][2]

    return black_board_list


def _lsa_start(black_board_list):
    lsa_reader = lsa_reader_instance
    count = len(black_board_list)
    _lsa_first_step(black_board_list[0])
    for i in range(0, count):
        _lsa_process(black_board_list[i], i+1)



def _lsa_first_step(black_board):
    lsa_reader = lsa_reader_instance
    rep_list = []
    for term in black_board.term_list:
        for i in range(0, term.frequency):
            rep_list.append(term.term)

    lsa_reader.index_document_LSA(rep_list, 0)
    lsa_reader.document_hashtable[0] = Article(black_board.article.activity,"Treinamento", "Treinamento", "Treinamento")

def _lsa_process(black_board, count):
    lsa_reader = lsa_reader_instance
    rep_list = []
    for term in black_board.lsa_term_list:
        if len(term.term) > 0:
            rep_list.append(term.term)
    
    lsa_reader.index_document_LSA(rep_list, count)
    lsa_reader.document_hashtable[count] = Article(
        black_board.article.activity, black_board.article.course,
        black_board.article.student, black_board.article.text)