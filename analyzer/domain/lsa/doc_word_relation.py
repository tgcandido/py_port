class DocWordRelation(object):
    def __init__(self):
        self.doc_id = 0
        self.pal_id = 0

    def __hash__(self):
        return hash(str(self.doc_id) + str(self.pal_id))

    def __eq__(self, other):
        return self.doc_id == other.doc_id and self.pal_id == other.pal_id