from .hash_obj import HashObj

class DocumentIndex(object):
    def __init__(self):
        self.doc_list = {}
        self.word_list = {}
        self.wtdm = {}
        self.inverse_skin = {}
        self.uk = {}
        self.v = {}