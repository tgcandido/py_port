import re
from analyzer.domain.term import Term

class DecomposerAgent(object):
    def __init__(self, black_board):
        self.black_board = black_board

    def decompose_message(self):
        delim = " |;|\'|´|`|\*|,|\n|\r|\t|}|{|-|_|=|\[|\]|&|\(|\)|/|\||:"
        str_arr = re.split(delim, self.black_board.message)
        token_list = []

        for token in str_arr:
            term = Term()
            term.term = token
            token_list.append(term)
        
        self.black_board.lsa_term_list = token_list
        self.black_board.tokens_count = len(token_list)
        self._create_dangerous_terms(token_list)

    def _create_dangerous_terms(self, term_list):
        dangerous_token_list = []

        for term in term_list:
            tk_term = next((x for x in self.black_board.term_list if x.term == term.term), None) 

            if tk_term is not None:
                dangerous_token = next((x for x in dangerous_token_list if x.term == tk_term.term), None) 
                if dangerous_token is None:
                    term.term = tk_term.term
                    term.frequency = tk_term.frequency
                    term.subject = tk_term.subject
                    term.text_frequency = 1
                    dangerous_token_list.append(term)
                else:
                    dangerous_token.text_frequency = ++dangerous_token.text_frequency
        
        self.black_board.dangerous_term_list = dangerous_token_list