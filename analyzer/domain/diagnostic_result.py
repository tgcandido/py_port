class DiagnosticResult(object):
    def __init__(self, subject, frequency_sum, count, concentration):
        self.subject = subject
        self.frequency_sum = frequency_sum
        self.count = count
        self.concentration = concentration
