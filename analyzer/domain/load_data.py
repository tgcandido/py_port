from analyzer.domain.term import Term
from trainning.models import Trainning

class LoadData(object):
    def __init__(self, black_board, subject):
        self.subject = subject
        self.black_board = black_board
        self._load_article_data()


    def _load_article_data(self):
        delim = '|'
        line = ''
        term_list = []

        t = Trainning.objects.get(subject = self.subject)

        if t is not None:
            tokens = t.tokens.split('\n')
            for token in tokens:
                info = token.split('|')
                if len(info) == 3:
                    term = Term(int(info[0]), info[1], info[2])
                    term_list.append(term)
        else:
            raise Exception()

        self.black_board.term_list = term_list