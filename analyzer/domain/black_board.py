
class BlackBoard(object):
    def __init__(self):
        self.tokens_count = 0
        self.term_list = []
        self.dangerous_term_list = []
        self.concentration = 0
        self.message = ''
        self.term_list_result = {}
        self.file_number = 0
        self.article = {}
        self.lsa_term_list = []
