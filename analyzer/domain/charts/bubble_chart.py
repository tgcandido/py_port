from .person import Person
from analyzer.models import BubbleChart
import json



def save_bubble_chart_data(uuid, black_board_list):
    for black_board in black_board_list:
        p1 = Person()
        p1.name = black_board.article.student
        p1.size = None
        p1.children = []

        p2 = Person()
        p2.name = black_board.article.activity
        p1.size = None
        p2.children = []

        for term in black_board.dangerous_term_list:
            p3 = Person()
            p3.name = term.term
            p3.size = str(term.frequency)
            p3.children = None
            p2.children.append(p3)
        p1.children.append(p2)

        chart = BubbleChart(uuid = uuid, chart = json.dumps(p1, default=lambda obj :  obj.__dict__))
        chart.save()

    