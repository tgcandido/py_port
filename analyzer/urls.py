from django.conf.urls import url
from analyzer import views

urlpatterns = [
    url(r'^$', views.analyzer, name='analyzer_index'),
    url(r'^results/(?P<id>[\w-]+)/$', views.results, name='results_index'),
    url(r'^json/bubble/(?P<id>[\w-]+)/$', views.bubble_chart_rest, name='json_results_index'),
]