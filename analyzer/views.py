import uuid
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from analyzer.domain.lsa.diagnoser import diagnose_article_list
from analyzer.domain.charts.bubble_chart import save_bubble_chart_data
from analyzer.domain.article_list import ArticleList
from .models import BubbleChart

@csrf_exempt
def analyzer(request):
    if request.method == 'POST' and request.FILES.getlist('files'):
        article_messages = []
        files = request.FILES.getlist('files')

        for file in files:
            file.seek(0)
            article_messages.append(
                file.read().decode('utf-8'))
            
        article_list = ArticleList(article_messages)
        black_board_list = diagnose_article_list(article_list)
        uuid_str = str(uuid.uuid4())

        save_bubble_chart_data(uuid_str, black_board_list)

        return HttpResponseRedirect('/analyzer/results/' + uuid_str)
    return render(request, 'analyzer/analyzer.html')


@csrf_exempt
def results(request, id):
    return render(request, 'analyzer/result.html', context={'id' : id})

@csrf_exempt
def bubble_chart_rest(request, id):
    bubble = BubbleChart.objects.get(uuid = id)
    if bubble is None:
        return HttpResponse(404)
    else:
        return HttpResponse(bubble.chart)