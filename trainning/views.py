from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django import template
from django.shortcuts import render
import datetime
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.edit import FormView
from trainning.domain.article import Article
import pickle

@csrf_exempt
def trainning(request):
    if request.method == 'POST' and request.FILES.getlist('files'):
        article_messages = []
        files = request.FILES.getlist('files')
        subject = request.POST['subject']
        for file in files:
            file.seek(0)
            article_messages.append(
                file.read().decode('utf-8'))
            
        article = _process_article(subject, article_messages)
        return HttpResponseRedirect('/analyzer')
    return render(request, 'trainning/trainning.html')


def _process_article(title, article_messages):
        article = Article(article_messages, title, 'pt-br')
        article.process()
        return article

        