import math
import re
from trainning.domain.stop_words import StopWords 
from trainning.domain.message_list import MessageList
from trainning.domain.message import Message
from trainning.domain.token_list import TokenList
from trainning.domain.token import Token
from trainning.models import Trainning

class Article(object):

    def __init__(self, article_messages, theme, language):
        self.article_messages = article_messages
        self.theme = theme
        self.language = language
        self.messages = MessageList()

    def process(self):
        self.article_list = self._initializeTokens()
        self.article_list.sort_by_ocurrence_number()
        self._calculate_message_concentration()
        self._calculate_standard_deviation()

        t = Trainning(subject = self.theme, tokens = str(self.article_list))
        t.save()



    def _initializeTokens(self):
        delimaux = 'x-uid'
        delimaux1 = 'x-keywords:'
        delimaux2 = " |;|\'|´|`|\*|,|\n|\r|\t|}|{|-|_|=|\[|\]|&|\(|\)|/|\||:"
        strs = []
        tokens = []

        for message in self.article_messages:
            withoutHeader = message.split(delimaux)
            if len(withoutHeader) == 1:
                withoutHeader = message.split(delimaux1)

            if len(withoutHeader) > 1:
                strs = list(filter(lambda x : x != '', re.split(delimaux2, withoutHeader[1])))
            else:
                strs = list(filter(lambda x : x != '', re.split(delimaux2, withoutHeader[0])))


            for token in strs:
                exists = False
                for i in range(0, len(tokens)):
                    if token == tokens[i].name:
                        tokens[i].frequency = tokens[i].frequency + 1
                        exists = True
                if exists == False:
                    if token.isdigit() is False:
                        newToken = Token(token, self.theme, 1)
                        tokens.append(newToken)

            stopWords = StopWords(self.language)
            return TokenList(stopWords.remove_stop_words(tokens))

    def _createStringFromTokens(tokens):
        text = ''
        for token in tokens:
            text = str(token.frequency) + '|' + token.name + '|' + token.theme + '\n'
        
        return text

    def _calculate_message_concentration(self):
        i = 1
        concentration = 0.0
        for article_message in self.article_messages:
            msg = Message(i, self._get_message_concentration(article_message))
            concentration += msg.concentration
            self.messages.messages.append(msg)
            i += 1
        
        self.messages.standard_deviation = concentration / i

    def _get_message_concentration(self, msg):
        delim = ' '
        msg_concentration = 0.0
        term_count = 1
        tokens = TokenList()

        for token in msg.split(delim):
            result = list(filter(lambda t: t.name == token, self.article_list.tokens))
            if  len(result) > 0 and result is not None:
                term_count += 1
                msg_concentration += result[0].frequency

        return msg_concentration / term_count

    def _calculate_standard_deviation(self):
        sum = 0.0
        for msg in self.messages.messages:
            sum += (msg.concentration - self.messages.standard_deviation)**2
        
        self.messages.standard_deviation = math.sqrt((sum/len(self.messages.messages)))