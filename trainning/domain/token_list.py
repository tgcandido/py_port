class TokenList(object):
    def __init__(self, tokens =[]):
        self.tokens = tokens

    def sort_by_ocurrence_number(self):
        self.tokens = sorted(self.tokens, key=lambda t : t.frequency, reverse=True)

    def __str__(self):
        mapped_list = map(lambda t : str(t.frequency) + '|' + str(t.name) + '|' + str(t.theme) + '\n', self.tokens)
        return ''.join(mapped_list)