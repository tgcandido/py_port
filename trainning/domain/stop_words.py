class StopWords(object):
    def __init__(self, language):
        if language == 'en-us':
            self.stopWords = ["A","GOING","GET","CERTAIN","EVER","BACK","ANYTHING","DE", "ANOTHER","RATHER","AWAY", "ABOUT","BETTER","AREN","EVEN","BEHIND","BESIDES",
            "BEYOND","HIGH","NEVER","ELSE","ENOUGH","HOWEVER","GOT","INSTEAD","LEAST","GREAT","LESS","LATER","JUST","ABOVE","MIGHT","PUT","MANY",
            "ONE","LITTLE","MAYBE","MUST","MUCH","NEITHER","NEW","OLD","TWO","NOTHING","OFTEN","SET","PERHAPS","SNT","SINCE","SEVERAL","ACCORDING",
            "THREE","SOON","SOMETHING","SOMETIMES","STILL","THEREFORE","YET","THING","THOUGH","TOWARDS","TOGETHER", "WHETHER","WHOSE", "WHOLE","US",
            "UPON","WITHOUT","ACROSS", "ACTUALLY", "ALTHOUGH","ALONG", "ALREADY","ALWAYS","AMONG", "ALSO","WHILE","OURS", "WHICH","E", "U", "S",
            "AFTER", "AGAIN", "AGAINST", "ALL", "ALMOST", "AM", "AN", "AND", "WITHIN", "ANY", "ARE", "AREN'T", "AS", "AT", "BE", "BECAUSE",
            "BEEN", "BEFORE", "BEING", "BELOW", "BETWEEN", "BOTH", "BUT", "BY", "CAN'T", "CANNOT", "COULD", "COULDN'T", "DID", "DIDN'T", "DO",
            "DOES", "DOESN'T", "DOING", "DON'T", "DOWN", "DURING", "EACH", "FEW", "FOR", "FROM", "FURTHER", "HAD", "HADN'T", "HAS", "HASN'T",
            "HAVE", "HAVEN'T", "HAVING", "HE", "HE'D", "HE'LL", "HE'S", "HER", "HERE", "HERE'S", "HERS", "HERSELF", "HIM", "HIMSELF", "HIS",
            "HOW", "HOW'S", "I", "I'D", "I'LL", "I'M", "I'VE", "IF", "IN", "INTO", "IS", "ISN'T", "IT", "IT'S", "ITS", "ITSELF", "LET'S", "ME",
            "MORE", "MOST", "MUSTN'T", "MY", "MYSELF", "NO", "NOR", "NOT", "OF", "OFF", "ON", "ONCE", "ONLY", "OR", "OTHER", "OUGHT", "OUR",
            "OURSELVES", "OUT", "OVER", "OWN", "SAME", "SHAN'T", "SHE", "SHE'D", "SHE'LL", "SHE'S", "SHOULD", "SHOULDN'T", "SO", "SOME", "SUCH",
            "THAN", "THAT", "THAT'S", "THE", "THEIR", "THEIRS", "THEM", "THEMSELVES", "THEN", "THERE", "THERE'S", "THESE", "THEY", "THEY'D", "",
            "THEY'LL", "THEY'RE", "THEY'VE", "THIS", "THOSE", "THROUGH", "TO", "TOO", "UNDER", "UNTIL", "UP", "VERY", "WAS", "WASN'T", "WE", "PLEASE",
            "WE'D", "WE'LL", "WE'RE", "MAY", "WILL", "WE'VE", "WERE", "WEREN'T", "WHAT", "WHAT'S", "WHEN", "WHEN'S", "WHERE", "WHERE'S","SUBJECT:", "SUBJECT", 
            "WHO", "WHO'S", "WHOM", "WHY", "WHY'S", "WITH", "WON'T", "WOULD", "WOULDN'T", "YOU", "YOU'D", "YOU'LL", "YOU'RE", "YOU'VE",
            "YOUR", "YOURS", "YOURSELF", "YOURSELVES","N'T", "LABEL", "KEYWORDS", "AUTOR", "ABSTRACT"] 
        elif language == 'pt-br':
            self.stopWords = ["A","À", "AGORA", "AINDA", "ALGUÉM", "ALGUM", "ALGUMA", "ALGUMAS", 
                    "ALGUNS", "AMPLA", "AMPLAS", "AMPLO", "AMPLOS", "ANTE", "ANTES", "AO", "AOS", "APÓS", "AQUELA", "AQUELAS", 
                    "AQUELE", "AQUELES", "AQUILO", "AS", "ATÉ", "ATRAVÉS", "CADA", "COISA", "COISAS", "COM", "COMO", "CONTRA", 
                    "CONTUDO", "DA", "DAQUELE", "DAQUELES", "DAS", "DE", "DELA", "DELAS", "DELE", "DELES", "DEPOIS", "DESSA", 
                    "DESSAS", "DESSE", "DESSES", "DESTA", "DESTAS", "DESTE", "DESTE", "DESTES", "DEVE", "DEVEM", "DEVENDO", "DEVER", 
                    "DEVERÁ", "DEVERÃO", "DEVERIA", "DEVERIAM", "DEVIA", "DEVIAM", "DISSE", "DISSO", "DISTO", "DITO", "DIZ", "DIZEM", 
                    "DO", "DOS", "E", "É", "ELA", "ELAS", "ELE", "ELES", "EM", "ENQUANTO", "ENTRE", "ERA", "ESSA", "ESSAS", "ESSE", 
                    "ESSES", "ESTA", "ESTÁ", "ESTAMOS", "ESTÃO", "ESTAS", "ESTAVA", "ESTAVAM", "ESTÁVAMOS", "ESTE", "ESTES", "ESTOU", 
                    "EU", "FAZENDO", "FAZER", "FEITA", "FEITAS", "FEITO", "FEITOS", "FOI", "FOR", "FORAM", "FOSSE", "FOSSEM", "GRANDE", 
                    "GRANDES", "HÁ", "ISSO", "ISTO", "JÁ", "LA", "LÁ", "LHE", "LHES", "LO", "MAS", "ME", "MESMA", "MESMAS", "MESMO", 
                    "MESMOS", "MEU", "MEUS", "MINHA", "MINHAS", "MUITA", "MUITAS", "MUITO", "MUITOS", "NA", "NÃO", "NAS", "NEM", 
                    "NENHUM", "NESSA", "NESSAS", "NESTA", "NESTAS", "NINGUÉM", "NO", "NOS", "NÓS", "NOSSA", "NOSSAS", "NOSSO", "NOSSOS", 
                    "NUM", "NUMA", "NUNCA", "O", "OS", "OU", "OUTRA", "OUTRAS", "OUTRO", "OUTROS", "PARA", "PELA", "PELAS", "PELO", 
                    "PELOS", "PEQUENA", "PEQUENAS", "PEQUENO", "PEQUENOS", "PER", "PERANTE", "PODE", "PUDE", "PODENDO", "PODER", 
                    "PODERIA", "PODERIAM", "PODIA", "PODIAM", "POIS", "POR", "PORÉM", "PORQUE", "POSSO", "POUCA", "POUCAS", "POUCO", 
                    "POUCOS", "PRIMEIRO", "PRIMEIROS", "PRÓPRIA", "PRÓPRIAS", "PRÓPRIO", "PRÓPRIOS", "QUAIS", "QUAL", "QUANDO", 
                    "QUANTO", "QUANTOS", "QUE", "QUEM", "SÃO", "SE", "SEJA", "SEJAM", "SEM", "SEMPRE", "SENDO", "SERÁ", "SERÃO", 
                    "SEU", "SEUS", "SI", "SIDO", "SÓ", "SOB", "SOBRE", "SUA", "SUAS", "TALVEZ", "TAMBÉM", "TAMPOUCO", "TE", "TEM", 
                    "TENDO", "TENHA", "TER", "TEU", "TEUS", "TI", "TIDO", "TINHA", "TINHAM", "TODA", "TODAS", "TODAVIA", "TODO",
                    "TODOS", "TU", "TUA", "TUAS", "TUDO", "ÚLTIMA", "ÚLTIMAS", "ÚLTIMO", "ÚLTIMOS", "UM", "UMA", "UMAS", "UNS", "VENDO", 
                    "VER", "VEZ", "VINDO", "VIR", "VOS", "VÓS", "ASSIM", "ÀS"]
        
    def remove_stop_words(self, tokens):
       return [x for x in tokens if x.name.upper() not in self.stopWords]
    
    def is_stop_word(self, query):
        if query.upper() in self.stopWords:
            return True
        else:
            return False
 