from django.conf.urls import url
from trainning import views

urlpatterns = [
    url(r'^$', views.trainning, name='trainning_index'),
]